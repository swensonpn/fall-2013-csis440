/**
 * @METHOD - Parses students and scores from a string evaluates whether a score is valid an calculates an average
 * @PARAM string - List of students and scores where student/score pairs are separated by a , and the name is separated from the socre with a :
 * @VOID 
 */
 function avg(str){
	//declare output variables
	var total=0;
	var validCount=0;
	var invalidArr=[];
 
	//Split string into an array where each element is a student:score string
	var studentArr = str.split(',');
	
	//Loop through students
	for(var i=0; i<studentArr.length; i++){
		
		//split 'name:score' into ['name',score] and assign back to studentArr[i]
		studentArr[i] = studentArr[i].split(':');
		
		//cast score as a float (need to keep decimal value)
		studentArr[i][1] = parseFloat(studentArr[i][1]);
		
		//Test if score is valid
		if(studentArr[i][1] > 100 || studentArr[i][1] < 0){//invalid
			invalidArr.push(studentArr[i].join(':'));
		}
		else{//valid
			total += studentArr[i][1];
			validCount++;
		}		
	}
	
	//Calculate Average and output	
	var avg = total/validCount;
	avg = avg.toFixed(2);//round to 2 decimal places
	console.log('Average for Test 1 is: ' + avg + '%');
	
	//Output Invalid Scores
	console.log("\rInvalid Scores:");
	for(i=0; i<invalidArr.length; i++){//i was declared above so does not need var 
		console.log("\t" + invalidArr[i]);
	}
 }
 
 //Grading input
 avg("Bev Johnson:93.6,Bob Green:0,Billy Green:-1,Sammy Samples:100,Sussy Simon:100.1,Ralph Nader:92.5,Gail Wilson:85.5,Shawn Singlton:99.9");