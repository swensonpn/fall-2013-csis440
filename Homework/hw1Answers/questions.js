document.writeln('<p>Question 1: ');
//question 1 code
function changeValue() {
    var msg = "world";
}
msg = "hello";
changeValue();
document.writeln(msg);


document.writeln('</p><p>Question 2: ');
//question 2 code
var val;
document.writeln(val);


document.writeln('</p><p>Question 3: ');
//question 3 code
document.writeln(parseInt("10", 4));


document.writeln('</p><p>Question 4: ');
//question 4 code
var a = 0x8;
var b = 0x1;
document.writeln(a | b);


document.writeln('</p><p>Question 5: ');
//question 5 code
var a = 1;
if (a == 2);
document.writeln("a is equal to 2");


document.writeln('</p><p>Question 6: ');
//quetion 6 code
var arr = [1, "two", true, false];
arr.splice(1, 1, "one");
document.writeln(arr.join(':'));


document.writeln('</p><p>Question 7: ');
//question 7 code
var func = new Function("a", "b", "return a + b;");
document.writeln(func("1", "2"));


document.writeln('</p><p>Question 8: ');
//question 8 code (changed func name to func1 to prevent conflict with #7)
function func1 (param1, param2, param3) {
    document.writeln(param3(param1, param2));
}
func1("1", "2", function(x, y) { return x + y;});


document.writeln('</p><p>Question 9: ');
//question 9 code (changed func name to func2 to prevent conflict with #7)
function func2 (arr) {
    arr.reverse();
}
var arr = new Array(1, 2, 3);
func2(arr);
document.writeln(arr.join(','));


document.writeln('</p><p>Question 10: ');
//question 10 code (changed func name to func3 to prevent conflict with #7)
function func3 (arr) {
    arr = new Array("one", "two");
}
var arr = new Array(1, 2);
document.writeln(arr);


document.writeln('</p>');