//Start Test Script for Object Two
console.log("\rStart Test Two:");
var obj1 = new EventObject({name:'Game Object 1',msg:''});
var obj2 = new EventObject({name:'Game Object 2',msg:''});
var handler = function(data){
    var fn = this;
    if(data.counter > 0){
        setTimeout(function(){
            if(data.counter%2 === 0){
                obj2.trigger('testevent',data);
            }else{
                obj1.trigger('testevent',data);  
            }
            fn.props('msg','The counter value is ' + data.counter);
            console.log(fn.props('name') + ': ' + fn.props('msg'));
            data.counter = data.counter-1;
        },(data.counter*100));
    }else{
        obj1.unbind('testevent',handler);
        obj2.unbind('testevent',handler);
        obj1.trigger('testevent',{counter:10});
    }
};
obj1.bind('testevent',handler);
obj2.bind('testevent',handler);
if(obj1 instanceof BaseObject && obj1 instanceof EventObject){
    console.log('Inherietance is properly setup');
    obj1.trigger('testevent',{counter:5});
}
else{
    console.log('Inherietance is NOT properly setup');
}