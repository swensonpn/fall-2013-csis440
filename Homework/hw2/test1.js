//Start Test Script for Object One
console.log("\rStart Test One:");
var bObj = new BaseObject({property1:'value1'});
if(bObj instanceof BaseObject){
    console.log("BaseObject Created");
    bObj.props('property2','value2');
    console.log('The value of property1 is: ' + bObj.props('property1'));
    console.log('The value of property2 is: ' + bObj.props('property2'));
}
else{
    console.log("BaseObject not successfully created");
}