//Start Test Script for Object Three
console.log("\rStart Test Three:");
var bob = new PersonObject({name:'Bob'});
var nancy = new PersonObject({name:'Nancy'});
if(bob instanceof PersonObject && bob instanceof EventObject && bob instanceof BaseObject){
    console.log("Inherietance is properly setup");
    bob.bind('onspeak',function(data){nancy.listen(data);});
    nancy.bind('onspeak',function(data){bob.listen(data);});
    bob.speak();
}
else{
    console.log('Inherietance is NOT properly setup');
}