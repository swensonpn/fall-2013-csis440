/**
 * AGENDA
 * 1. Proposals turned in on bitbucket
 * 2. Activity 5 (13 and 14) for Tuesday
 * 3. Lecture - Custom objects
 */

/* FUNCTIONS AS OBJECTS
 *  Everything in JavaScript is an object, and functions can be utilized to create reusable objects.
 */
//Declare a function show how the new operator makes an object
var MyObject = function(){
   
};

var mO1 = MyObject();//Calls function
var mO2 = new MyObject();//creates an instance

// view results in the console to see how the differ
console.log(mO1);
console.log(mO2);
 
/* CALLING VS. CREATING AN INSTANCE
 *  As we see above calling our object incorrectly can give the wrong result
 */
// Show how to force creation of an object
var MyForcedObject = function(param1){
    if(!(this instanceof MyForcedObject)){
        return new MyForcedObject(param1);
    }
}

var mO3 = MyForcedObject('param1');//call as function
var mO4 = new MyForcedObject('param1');//call with  new

// view in console to see they are the same
console.log(mO3);
console.log(mO4);
 
 
/* CONSTRUCTOR
 * When we create an instance of an object the entire function is called.  So essentially anything not in a nested function is part of the constructor.
 */
//Show that the function body code gets run when using new
var Object1 = function(){
    console.log(arguments[0]);
    console.log(arguments[1]);
    console.log(arguments.length);//We can still use the arguments collection when making objects
}
var obj1 = new Object1('Hello','CSIS 440','this i thew in for fun');


/* EXTEND OBJECT WITH PROTOTYPE
 *  We can give our object more properties and methods using prototype
 */
 //Extend MyObject (first one we made)
 MyObject.prototype.property1 = "Value of property 1";
 MyObject.prototype.method1 = function(msg){
     console.log(msg);
 }
 
//can call properties and methods with dot opertator
var mO5 = new MyObject();
console.log(mO5.property1);
mO5.method1('This is a message');

 
 

/* THIS KEYWORD 
 *  Using the this keyword to declare variables and functions creates public properties and methods.
 *  All variables and functions declared with var are private
 */
//Declare a more complete object
var Shape = function(sides,color){
    var numSides = sides;
    
    this.color = color;
    
    this.getSides = function(){
        return numSides;  
    };
};
var shape1 = new Shape(3,'blue');

//Try to access a public property
console.log(shape1.color);

//Try to access a var property
   console.log(shape1.numSides); //undifined
   console.log(shape1.getSides());// gives access to sides
 
//Try to access a private property inside a prototype (notice you can use numSides inside getSides method)

    //Private variables are only available if functions are nested

//Show that this can be assigned to a variable
var Object1 = function(){
    var obj = this;
};

 
/* INHERIETANCE WITH CALL AND APPLY
 *  There is more than one way to extend objects in JavaScript.  We can use call and apply to call a function in the context of another
 */
// Example of call
var Rectangle = function(color,h,w){
    this.height = h;
    this.width = w;
    Shape.call(this,4,color);
};

var rect1 = new Rectangle('red',10,5);
console.log(rect1);

// Example of apply
var Triangle = function(color,s1,s2,s3){
    this.side1 = s1;  
    this.side2 = s2;  
    this.side3 = s3;  
    this.test = function(){return numSides;};
    var arr = [3,color];
    Shape.apply(this,arr);
};
var tri1 = new Triangle('orange',5,10,5);
console.log(tri1);
console.log(tri1.getSides());

/***************************************************/
//Show that instanceof doesn't work
if(tri1 instanceof Shape){
    console.log("tri1 is a shpape");
}
else{
    console.log("tri1 is not a shape");
}
 
/* INHERIETANCE WITH CONSTRUCTOR AND PROTOTYPE
 *  We can also use the constructor and prototype properties to extend objects.  This allows us to test if on object is an extension of another
 */
//Example that works with instanceof
var Square = function(color,w){
    Rectangle.call(this,color,w,w);
};
Square.prototype = Object.create(Rectangle.prototype);
Square.constructor = Rectangle;
 
 var sq1 = new Square('green',5);
 
 if(sq1 instanceof Rectangle){
     console.log("sq1 is a rectangle");
}
else{
    console.log("sq1 is not rectangle");
}
 
 /* PUBLIC VS PRIVATE VS PROTECTED
  *     We showed how var makes things private and how this makes things public.  How can we make things protected
  *     or change their access level in objects inherieting from another.
  */
//Reasign things to make alter their access level
var BaseObject = function(param1,param2,param3){
    var p1 = param1;
    this.p2 = param2;
    this.p3 = param3;
    console.log(param1+param2+param3);
};
var bO1 = new BaseObject('val1','val2','val3');
console.log('param1=' +bO1.p1);
console.log('param2=' +bO1.p2);
console.log('param3=' +bO1.p3);
  
var ExtendedObject = function(param1,param2,param3){
    var obj = this;
    BaseObject.apply(this,arguments);
    return new function(){
        this.p3 = obj.p3;
        console.log(param1+param2+param3);
    }
};
var eO1 = new ExtendedObject('val1','val2','val3');
console.log('param1=' +eO1.p1);
console.log('param2=' +eO1.p2);
console.log('param3=' +eO1.p3);
 