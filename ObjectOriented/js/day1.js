/**
 * AGENDA
 *  1. Activity 4
 *  2. Due dates approaching for Homework 1 and Project Proposal
 *  3. Prototypes and Direct Instance Objects
 */
 
/*
 * PROTOTYPE
 * JavaScript is Object Based not Object Oriented.  In a nutshell everything in JavaScript is an object,
 * but we do not have the class structure normally thought of in object oriented programming.  JavaScript's
 * flavor of object oriented programming is known as prototypical.  This means every object (everything is an object)
 * has a collection property called prototype.  To extend an object (even a native one) we simply add the new
 * property or method to its prototype.
 */
 
 //Extending Array. 
 var arr1 = [1,2,3];
 for(var n in arr1){console.log(n + '=' + arr1[n]);}//This demonstrates that we can use a for in loop to iterate over an array.
 
 Array.prototype.each = function(fn){
     for(var i=0; i<this.length; i++){
         this[i] = fn(this[i]) || this[i];
     }
 };
 
 arr1.each(function(item){return item * 5;});
 for(var n in arr1){console.log(n + '=' + arr1[n]);}//Notice that having extended Array changed the behavior of our for in loop.
 
 
/*
 * DIRECT INSTANCES 
 * Like arrays and functions we can create instances of objects using the new keyword.  We can also use
 * curly braces as a type of shorthand.  Objects created like this are sometimes called one off objects,
 * because we cannot use them as create new instances like we would a Java Class.  These are a great
 * when you only need one copy of the object.
 */
 
 // Show object creation using new and {}. 
 var obj1 = new Object();
 var obj2 = {};
 
 
 //Setting and Getting from Object.
 obj1.prop1 = 'value of objec1 property1';
 obj2.prop1 = 'value of object2 propterty1';
 console.log(obj1.prop1 + obj2.prop1);
 
 
 //You can set and get properties using notation similar to Arrays.
 obj1['prop2'] = "value of object1 property 1";
 
 var propName = 'prop2';
 obj2[propName] = "value of Object 2 unknown propert";
 
 for(var n in obj2){console.log(n + ':' + obj2[n]);}


 //Show can assign functions and other objects to the properties of an object
 obj2.arr = [4,5,6];
 obj2.func1 = function(prop){console.log(this[prop]);};
 obj2.obj3 = {};
 
 obj2.func1('arr');
 obj2['func1']('prop1');
 obj2.func1('test');
// obj2.test.func1();
 
 
 //Object notation can be used to fully describe an object in one closure
 var obj3 = {
     string1:'string value',
     number1:5,
     bool1:false,
     none:undefined,
     func1:function(param1){
         return this.number1;
     }
 };
 
 
 //All members of obj3 are public
 console.log(obj3.func1());
 obj3.func1 = 'x';
 //console.log(obj3.func1());
 
 
 //Namespacing
 var NBK = {
   utility:{},
   animation:{},
   crypt:{}
 };
 
 NBK.utility.getURL = function(){
     return window.location.href;
 };
 
 //Demonstrate using an init function as a constructor (singleton)
 var calc = {
     result:0,
    addition:function(){
        this.result += prompt('number?'); 
        this.doMath();
    },
    subtract:function(){
        this.result -= prompt('number?'); 
        this.doMath();
    },
    divide:function(){
        this.result /= prompt('number?'); 
        this.doMath();
    },
    multiply:function(){
        this.result *= prompt('number?'); 
        this.doMath();
    },
    remainder:function(){
        this.result %= prompt('number?'); 
        this.doMath();
    },
    getResult:function(){
        console.log(this.result);
        this.initialize();
    },
    doMath:function(){
        var type = prompt('What type of math?');
        this[type]();
    },
    initialize:function(){
        this.result = 0;
        this.doMath();
    }
 };
 
 (function(){
     calc.initialize();
     
 }());
 
 
 
 
 
 
 
 
 