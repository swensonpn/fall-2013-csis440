/**
 * AGENDA
 *  1.Proposals
 *  2.Activity 5 due today
 *  3.Homework 2 due 10/8
 *  4.Finish Objects
 *  5.Namespacing
 */


/*
 * NAMESPACING - Having many variables declared in the global scope can cause conflicts when code from more than one source
 *  is included in an application/page.  To reduce the probability of conflicts JavaScript code is commonly namespaced.
 *  In other words code is wrapped inside closures i.e.{} so that no more than one variable exposed to the global scope.
 */


/**
 * NAMESPACING WITH LITERAL/ONE-OFF OBJECTS - Literal objects make a convenient structure for organizing code.  
 *  Advantage
 *  - easy to read
 *  Disadvantage 
 *  - hard to change
 */
 //Example of an API using literal objects
var api1 = {
    settings:{},//A location that allows default values to be stored in a single location
    functions:{},//Function library for this application
    objects:{}//Could hold all the custom objects for this application
};
 
api1.objects.BaseObject = function(){/*object code here*/};//Items are declared individually like this.
 
//example of literal namespace scheme where renaming is less of an issue but the code is harder to read
var api2 = {
    settings:{
        framerate:30,//all code defined inside the closures
        messages:{
            newGame:'Would you like to play a new game?',
            gameOver:'Game Over.  Play again?'
        }
    },
    functions:{
        displayMessage:function(msgName){
            return confirm(api2.settings.messages[msgName]);
        }
    },
    objects:{
        
    }
};
api2.functions.displayMessage('newGame');
 
 
/**
 * NAMESPACING WITH FUNCTIONS- Like with object oriented programming functions are a powerful tool for namespacing
 *  Advantages
 *  - More Flexible
 *  - Allow for private variables
 * Disadvantages
 *  - harder to read
 */
 //Example of self-executing function 
(function(){
    /* application code goes here. Nothing is exposed to the window object.*/
}());


//Module Pattern - advantage is that the namespace name can be set dynamically, but in large applications this can be hard to read.
var api3 = (function(){
    var settings = {//Setting can now be protected from outside manipulation
        framerate:30,
        startLevel:1
    };
    this.getSetting=function(name){//Can have setters and getters
        return settings[name];
    };
    this.functions={};//Techniques can be combined
    this.objects=(function(){
        //second level namespace using module pattern
    }());
}());

//Passing in a context - same as above, but now you can inject at any level
var api4={};
(function(context){
     var settings = {//Setting can now be protected from outside manipulation
        framerate:30,
        startLevel:1
    };
    context.getSetting=function(name){//Can have setters and getters
        return settings[name];
    };
    context.functions={};//Techniques can be combined
    context.objects=(function(){
        //second level namespace using module pattern
    }());
}(api4));

//Another way to pass in context
var api5={};
(function(){
     var settings = {//Setting can now be protected from outside manipulation
        framerate:30,
        startLevel:1
    };
    this.getSetting=function(name){//Can have setters and getters
        return settings[name];
    };
    this.functions={};//Techniques can be combined
    this.objects=(function(){
        //second level namespace using module pattern
    }());
}).apply(api5);

//Yet another way to do this.  It exposes more variables to the global scope so perhaps better for second/third level namespaces or if initialization code wrapped in a self-executing function
var apiModule1={};
var apiModule2={};
var Module = function(){
     var settings = {//Setting can now be protected from outside manipulation
        framerate:30,
        startLevel:1
    };
    this.getSetting=function(name){//Can have setters and getters
        return settings[name];
    };
    this.functions={};//Techniques can be combined
    this.objects=(function(){
        //second level namespace using module pattern
    }());
};

Module.apply(apiModule1);
Module.call(apiModule2);