/**
 * @author Paul N Swenson
 * @version 1.0
 * CONFIGURATION This is a central location to store variables that control
 * attributes of the application execution.  This may be less efficient than 
 * defining settings in the object that uses them, but it makes them easier to
 * find later.
 */
(function(){
    var cfg = this;//Used to access the main context inside nested objects
    
    this.framerate = 30;
    
    this.canvases = [
        'terrain-canvas',
        'improve-canvas',
        'unit-canvas'
    ];
    
    this.imagePath = 'bin/';
    
    
    this.spriteSheets = {
        grass:{
            file:'tpack0/grass.png',
            sprites:[
                {name:'tile',startClipX:0,startClipY:0,clipWidth:50,clipHeight:50,scale:1} 
            ]
        },
        hill:{
            file:'tpack0/hill.png',
            sprites:[
                {name:'tile',startClipX:0,startClipY:0,clipWidth:50,clipHeight:50,scale:1} 
            ]
        },
        mountain:{
            file:'tpack0/mountain.png',
            sprites:[
                {name:'tile',startClipX:0,startClipY:0,clipWidth:50,clipHeight:50,scale:1} 
            ]
        },
        water:{
            file:'tpack0/water.png',
            sprites:[
                {name:'tile',startClipX:0,startClipY:0,clipWidth:50,clipHeight:50,scale:1} 
            ]
        },
        explode:{
            file:'explode.png',
            sprites:[
                {name:'step1',startClipX:0,startClipY:0,clipWidth:100,clipHeight:100,scale:1},
                {name:'step2',startClipX:110,startClipY:0,clipWidth:100,clipHeight:100,scale:1},
                {name:'step3',startClipX:220,startClipY:0,clipWidth:100,clipHeight:100,scale:1},
                {name:'step4',startClipX:330,startClipY:0,clipWidth:100,clipHeight:100,scale:1},
                {name:'step5',startClipX:440,startClipY:0,clipWidth:100,clipHeight:100,scale:1}    
            ]
        },
        city:{
            file:'city.png',
            sprites:[
                {name:'blueCity',startClipX:14,startClipY:8,clipWidth:55,clipHeight:120,scale:1},
                {name:'redCity',startClipX:78,startClipY:8,clipWidth:55,clipHeight:120,scale:1},
                {name:'mounatin',startClipX:170,startClipY:50,clipWidth:60,clipHeight:60,scale:1},
                {name:'trees',startClipX:250,startClipY:50,clipWidth:60,clipHeight:60,scale:1},
                {name:'aim',startClipX:315,startClipY:28,clipWidth:120,clipHeight:120,scale:1},
                {name:'cursor',startClipX:450,startClipY:28,clipWidth:120,clipHeight:120,scale:1} 
            ]
        },
        heloMan:{
            file:'helo-man.png',
            sprites:[
                {name:'blueHeloLeft',startClipX:14,startClipY:8,clipWidth:63,clipHeight:46,scale:0.5},
                {name:'blueHeloUp',startClipX:77,startClipY:8,clipWidth:52,clipHeight:46,scale:0.5},
                {name:'blueHeloRight',startClipX:135,startClipY:8,clipWidth:63,clipHeight:46,scale:0.5},
                {name:'blueHeloDown',startClipX:198,startClipY:8,clipWidth:60,clipHeight:55,scale:0.5},
                
                {name:'blueManLeft',startClipX:291,startClipY:7,clipWidth:50,clipHeight:60,scale:.5},
                {name:'blueManUp',startClipX:347,startClipY:8,clipWidth:50,clipHeight:60,scale:0.5},
                {name:'blueManRight',startClipX:425,startClipY:8,clipWidth:60,clipHeight:55,scale:0.5},
                {name:'blueManDown',startClipX:485,startClipY:8,clipWidth:60,clipHeight:55,scale:0.5},
                
                {name:'redHeloLeft',startClipX:14,startClipY:82,clipWidth:63,clipHeight:46,scale:0.5},
                {name:'redHeloUp',startClipX:77,startClipY:82,clipWidth:52,clipHeight:46,scale:0.5},
                {name:'redHeloRight',startClipX:137,startClipY:82,clipWidth:63,clipHeight:46,scale:0.5},
                {name:'redHeloDown',startClipX:201,startClipY:78,clipWidth:63,clipHeight:57,scale:0.5},
                
                {name:'redManLeft',startClipX:292,startClipY:74,clipWidth:50,clipHeight:60,scale:0.5},
                {name:'redManUp',startClipX:349,startClipY:74,clipWidth:60,clipHeight:55,scale:0.5},
                {name:'redManRight',startClipX:425,startClipY:74,clipWidth:60,clipHeight:55,scale:0.5},
                {name:'redManDown',startClipX:485,startClipY:74,clipWidth:60,clipHeight:55,scale:0.5}
            ]
        },
        tankArtillery:{
            file:'tank-artillery.png',
            sprites:[
                {name:'blueArtLeft',startClipX:14,startClipY:8,clipWidth:60,clipHeight:60,scale:0.5},
                {name:'blueArtUp',startClipX:78,startClipY:8,clipWidth:60,clipHeight:60,scale:0.5},
                {name:'blueArtRight',startClipX:142,startClipY:8,clipWidth:60,clipHeight:60,scale:0.5},
                {name:'blueArtDown',startClipX:206,startClipY:8,clipWidth:60,clipHeight:60,scale:0.5},
                
                {name:'blueTankLeft',startClipX:292,startClipY:8,clipWidth:60,clipHeight:60,scale:0.5},
                {name:'blueTankUp',startClipX:356,startClipY:8,clipWidth:60,clipHeight:60,scale:0.5},
                {name:'blueTankRight',startClipX:420,startClipY:8,clipWidth:60,clipHeight:60,scale:0.5},
                {name:'blueTankDown',startClipX:484,startClipY:8,clipWidth:60,clipHeight:60,scale:0.5},
                
                {name:'redArtLeft',startClipX:14,startClipY:78,clipWidth:60,clipHeight:60,scale:0.5},
                {name:'redArtUp',startClipX:78,startClipY:78,clipWidth:60,clipHeight:60,scale:0.5},
                {name:'redArtRight',startClipX:142,startClipY:78,clipWidth:60,clipHeight:60,scale:0.5},
                {name:'redArtDown',startClipX:201,startClipY:78,clipWidth:60,clipHeight:60,scale:0.5},
                
                {name:'redTankLeft',startClipX:294,startClipY:78,clipWidth:60,clipHeight:60,scale:0.5},
                {name:'redTankUp',startClipX:358,startClipY:78,clipWidth:60,clipHeight:55,scale:0.5},
                {name:'redTankRight',startClipX:422,startClipY:78,clipWidth:60,clipHeight:55,scale:0.5},
                {name:'redTankDown',startClipX:486,startClipY:78,clipWidth:60,clipHeight:55,scale:0.5}
            ]
        }
    };
    
    this.map = {
        width:30,
        height:20,
        dia:50
    };
    
    this.terrain = {
        grass:{move:1,see:1,attack:1,defend:0.7,resource:3,probability:.6,canvas:0,spriteSheet:'grass',sprites:['tile'],defaultSprite:'tile'},
        hill:{move:0.7,see:0.3,attack:0.7,defend:1,resource:2,probability:.2,canvas:0,spriteSheet:'hill',sprites:['tile'],defaultSprite:'tile'},
        mountain:{move:0.3,see:0,attack:0.3,defend:1.2,resource:1,probability:.1,canvas:0,spriteSheet:'mountain',sprites:['tile'],defaultSprite:'tile'},
        water:{move:0,see:1,attack:0,defend:0,resource:2,probability:.1,canvas:0,spriteSheet:'water',sprites:['tile'],defaultSprite:'tile'}
    };
    
    this.tTypeArr = (function(){
        var arr = [];
        for(var n in cfg.terrain){
            var times = cfg.terrain[n].probability * 10;
            for(var i=0; i<times; i++){
                arr.push(n);
            }
            //arr.push(n);
        }
        return arr;
    }());
    
    this.improvement = {
        city:{move:0,see:0,attack:0,defend:1,size:1,canvas:1,spriteSheet:'city',spritesBlue:['blueCity'],spritesRed:['redCity']},    
    };
    
    this.unit = {
        armor:{move:2,see:2,attack:3,defend:2,image:3,canvas:2,spriteSheet:'tankArtillery',spritesBlue:['blueTankLeft','blueTankUp','blueTankRight','blueTankDown'],spritesRed:['redTankLeft','redTankUp','redTankRight','redTankDown']},
        artillery:{move:2,see:2,attack:3,defend:1,image:3,canvas:2,spriteSheet:'tankArtillery',spritesBlue:['blueArtLeft','blueArtUp','blueArtRight','blueArtDown'],spritesRed:['redArtLeft','redArtUp','redArtRight','redArtDown']},
        aviation:{move:4,see:4,attack:3,defend:1,image:2,canvas:2,spriteSheet:'heloMan',spritesBlue:['blueHeloLeft','blueHeloUp','blueHeloRight','blueHeloDown'],spritesRed:['redHeloLeft','redHeloUp','redHeloRight','redHeloDown']},
        infantry:{move:1,see:1,attack:1,defend:2,image:2,canvas:2,spriteSheet:'heloMan',spritesBlue:['blueManLeft','blueManUp','blueManRight','blueManDown'],spritesRed:['redManLeft','redManUp','redManRight','redManDown']}
    };
    
}).apply(app.config);