/**
 * OBJECTLIBRARY This is the central location for all game application objects.
 * It contains not only the main actors but any ulitity objects that might be
 * needed to get the application running.
 */
(function(){
    var obj = this;//Set a variable that can be used to refer to this context.
    
    /**
     * Base - The object from which most game obects will extend
     * @param attributes an object containing any attributes this object should posses
     * @return void
     */
    this.Base = function(attributes){
        var attributes = attributes || {};
        
        //Public Properties
        this.cfg = app.config;
        this.fn = app.funcLib;
        this.obj = app.objLib;
        
        /**
         * Public Method attr sets or gets the value of an object property
         * @param name string indicating the property to set or get.
         * @param value any value that should be saved to attributes.
         * @return the value located at index name
         */
        this.attr = function(name,value){
            if(value === undefined){
                return attributes[name]  ;
            } 
            attributes[name] = value;
        };
    };
    
    
    /**
     * Interactive This core object extends base and add functionality for custom event handling
     * @param attributes an object containing any attributes this object should posses
     * @return void
     */
    this.Interactive = function(attributes){
        obj.Base.call(this,attributes);
        
        //Private Variable Declaration
        var events = {};
        
        /**
         * Public method bind Attach and event handler to one of this objects event types
         * @param type string type of event to attach
         * @param fn event hander function that is to be executed
         * @return void
         */
        this.bind = function(type,fn){
            if(typeof fn != 'function'){
                return false;
            }
            if(!events[type]){
                events[type] = [fn];
            }
            else{
                events[type].push(fn);
            }
        };
        
        /**
         * Public Method trigger fire all events attached to this objects event type
         * @param type the event type to call handlers on
         * @param data any additiona information wanted to pass to the handler function
         * @return void
         */
        this.trigger = function(type,data){
            if(events[type]){
                for(var i=0; i<events[type].length; i++){
                    events[type][i].call(this,data);
                }
            }
        };
        
        /**
         * Public Method unbind removes an event handler from the specified event type
         * @param type the type of this event
         * @param the hander function that should be removed
         * @return void
         */
        this.unbind = function(type,fn){                                        
            if(events[type]){                                                   
                for(var i=0; i<events[type].length; i++){
                    if(events[type][i] == fn){                                  
                        events[type].splice(i,1);                               
                    }
                }
            }
        };
    };
    this.Interactive.prototype = Object.create(this.Base.prototype);
    this.Interactive.constructor = this.Base;
    
    /**
     * Renderable Objects of this type can be rendered on the viewable screen
     * @param attributes an object containing any attributes this object should posses
     * @param address location of the object
     * @return void
     */
    this.Renderable = function(attributes,address){
        obj.Interactive.call(this,attributes);

        //Declare Private Variables
        var o = this;//variable to access context
        var currentAddress = address;
        var dia = this.cfg.map.dia;
        var ctx = app.config.canvases[o.attr('canvas')];
        var sheet = app.config.spriteSheets[o.attr('spriteSheet')];
        var sprite;

        //Public function for drawing the image on the canvas 
        o.render = function(newCoords){
            if(!sprite){
                sprite = sheet.getSprite(o.attr('defaultSprite'));  
            }
            
            var coords = (newCoords) ? newCoords : o.fn.getHexCoordsForAddr(dia,currentAddress);
            if(sprite){
                if(newCoords){
                    sprite.moveTo(ctx,coords[0],coords[1]);
                }
                else{
                    sprite.draw(ctx,coords[0],coords[1]);
                }                    
            }
        };


        /**
         * Public method addr get or set the objects location
         * @param address if present sets address
         * @return address after any assignments
         */
        o.addr = function(address){ 
            if(address){
                currentAddress = address;
                o.render(o.fn.getHexCoordsForAddr(dia,currentAddress));
            }
            return currentAddress;
        };
        
        o.setSprite = function(spriteName){   
            var sprt = sheet.getSprite(spriteName);
            if(sprt) sprite = sprt;
        };
        
        //Render if possible
        if(sheet.loaded){
            o.render();   
        }
    };
    this.Renderable.prototype = Object.create(this.Interactive.prototype);
    this.Renderable.constructor = this.Interactive;
    
    /**
     * Sprite - Object holds information about the part of an image to use and an easy way to render it
     * @param object obj - private attributes required by canvas to draw an image
     * @return void
     */
    this.Sprite = function(obj){ 
        var img = obj.image;
        var sx = obj.startClipX;
        var sy = obj.startClipY;
        var sw = obj.clipWidth;
        var sh = obj.clipHeight;
        var dx;
        var dy;
        var dw = obj.clipWidth*obj.scale;
        var dh = obj.clipHeight*obj.scale;
        
        var getLeft = function(x){
            return x-(dw/2);
        };
        
        var getTop = function(y){
            return y-(dh/2);
        };
        
        this.erase = function(canvas){
            canvas.clearRect(dx,dy,dw,dh);
        };
        
        this.moveTo = function(canvas,x,y){
            this.erase(canvas);
            this.draw(canvas,x,y);
        };
        
        this.number = obj.number; 
        
        this.draw = function(canvas,x,y){
            dx = getLeft(x);
            dy = getTop(y);
            canvas.drawImage(img,sx,sy,sw,sh,dx,dy,dw,dh);
        };
    };
    
    /**
     * SpriteSheet - Provides a way to break up image files into their individual sprites
     * @param string filename - filename of the sprite sheet
     * @param object spriteConfig - a singleton object with clipping, and scaling information
     * @return void
     */
    this.SpriteSheet = function(filename,spriteConfig){
        var o=this,img,sprites = {};
        
        this.loaded = false;
        
        this.getSprite = function(name){ 
            return sprites[name];
        };
        
        //This is our constructor code
        img = new Image();
        img.addEventListener('load',function(){
            o.loaded = true;
            for(var i=0; i<spriteConfig.length; i++){ 
                spriteConfig[i].image = img;
                spriteConfig[i].number = i;
                sprites[spriteConfig[i].name] = new obj.Sprite(spriteConfig[i]);
            }
        },false);
        img.src = filename;
    };
    
    /**
     * Animatable - This specialized object handles details about how to animate an object 
     *  @param
     *  @return
     */
    this.Animatable = function(attributes,address){
        obj.Renderable.call(this,attributes,address);
        
        //Private Variables
        var o = this;
        var dia = this.cfg.map.dia;
        var wait = false;
        var tweenTypes = {
            'default': [1,2,3,4,5,6,7,8,9,10,9,8,7,6,5,4,3,2,1],
		    'blast': [12,12,11,10,10,9,8,7,6,5,4,3,2,1],
		    'linear': [10,10,10,10,10,10,10,10,10,10]
        };
        var currentCoords=o.fn.getHexCoordsForAddr(dia,address);
        var nextAddress,framesRemaining,coordDiff=[];
        var type = o.attr('tweenType') || 'default';
        
        o.attr('tweenType',tweenTypes[type]);
        o.attr('ontween', function(){ 
            currentCoords[0] += coordDiff[0] * o.attr('tweenType')[o.attr('framesRemaining')-1] * 0.01;
            currentCoords[1] += coordDiff[1] * o.attr('tweenType')[o.attr('framesRemaining')-1] * 0.01;
            o.render(currentCoords);
        });
        o.attr('oncomplete',function(){
            o.addr(nextAddress);
            wait = false;
        });
        
        
        /**
         * Public Method - Move the element to a location by animation
         * @param address - new address
         * @return void
         */
        o.animateTo = function(address){ 
            if(wait) return;
            var nextCoords = this.fn.getHexCoordsForAddr(dia,address);
           
            nextAddress = address;
            currentCoords = this.fn.getHexCoordsForAddr(dia,this.addr());
            coordDiff[0] = nextCoords[0] - currentCoords[0];
            coordDiff[1] = nextCoords[1] - currentCoords[1];
           
            o.attr('framesRemaining', o.attr('tweenType').length);
            o.bind('tween',o.attr('ontween'));
            o.bind('complete',o.attr('oncomplete'));
            wait = true;
            app.timer.animate(o);
        };
    };
    this.Animatable.prototype = Object.create(this.Renderable.prototype);
    this.constructor = this.Renderable;
    
    /**
     * Terrain An object representation of one space on the game board.  Holds
     * data which affects other objects.
     * @param type what type of terrain is this tile
     * @param location of the object
     * @return void
     */
    this.Terrain = function(type,address){
         if(!app.config.terrain[type]){//not a valid terrain type
             return false;
         }
         var attributes = app.config.terrain[type];
         obj.Renderable.call(this,attributes,address);
    };
    this.Terrain.prototype = Object.create(this.Renderable.prototype);
    this.Terrain.constructor = this.Renderable;
    
    /**
     * Improvement This object is for man made items on a game board tile.  
     * They alter the attributes of the terrain they lie on and can be extended for
     * extra functionality
     * @param type name of the improvement.
     * @param address starting location of the object
     * @return void
     */
    this.Improvement = function(type,address){
        if(!app.config.improvement[type]){
             return false;
         }
         obj.Renderable.call(this,app.config.improvement[type]);
    };
    this.Improvement.prototype = Object.create(this.Renderable.prototype);
    this.Improvement.constructor = this.Renderable;
    
    /**
     * City is a type of improvement.  Cities produce output and grow by utilizing
     * the resources from terrain.
     * @param address the starting location of this object
     * @return void
     */
    this.City = function(address){
        obj.Improvement.call(this,'city');
    };
    
    /**
     * Unit is the basic object that conducts war.  Each unit may move, attack, and
     * destroy improvements.
     * @param type the unit type
     * @param address starting location
     * @return void
     */
    this.Unit = function(player, type,address){
        if(!app.config.unit[type]){
             return false;
         }
         var o = this;
         var team = (player === 1) ? 'Blue' : 'Red';
         var dia = app.config.map.dia;
         var coords = app.funcLib.getHexCoordsForAddr(dia,address);
         var nextCoords,coordDiff,attr={};
         var tmp = app.config.unit[type];
         var movement = 0;
         
         attr.spriteArr = tmp['sprites'+team];
         attr.defaultSprite = attr.spriteArr[0];
         attr.spriteSheet = tmp.spriteSheet;
         attr.move = tmp.move;
         attr.see = tmp.see;
         attr.attack = tmp.attack;
         attr.defend = tmp.defend;
         attr.image = tmp.image;
         attr.canvas = tmp.canvas;
         obj.Animatable.call(this,attr,address);
         
         /**
          * Public Method getId - get a unit id to help identify it on the screen
          */
        o.getId = function(){
            return team + '-' + o.attr('defaultSprite') + ':' + o.addr();
        };
        
        /**
         * Public Method - Reset movement
         *  @param void
         *  @return void
         */
        o.resetMovement = function(){
            movement = o.attr('move');
        };
         
         /**
          * Public Method move - move the selected unit.  Attacks happen as event of occupying a space on the map
          * @param address the space to move to.
          * @return boolean false if cannot move true otherwise
          */
        o.moveTo = function(addrPart1,addrPart2){
            var curAddr = this.addr().split(':');
            var newAddr = addrPart1 + ':' + addrPart2;
            var sprArr = o.attr('spriteArr');
            
            //Ensure the this move is in bounds
            if(addrPart1 < 0 | addrPart1 >= app.config.map.width || addrPart2 < 0 || addrPart2 >= app.config.map.height || app.map[newAddr].attr('move') === 0){
                console.log('illegal move');
                return;
            }

            //Set the correct Sprite Image
            if(curAddr[1] == addrPart2){
                (curAddr[0] > addrPart1) ? o.setSprite(sprArr[0]) : o.setSprite(sprArr[2]);
            }
            else{
                (curAddr[1] > addrPart2) ? o.setSprite(sprArr[1]) : o.setSprite((sprArr[3]));
            }
            
            //Test if Movement is left before animating
            if(movement > 0){
                //Start the animation
                o.animateTo(addrPart1+':'+addrPart2);
                movement = (o.attr('move') * app.map[newAddr]) - 1;//Use movement and terrain to control how far we can go
                return (movement > 0) ? true : false;
            }
            return false;
        };
        
        
    };
    this.Unit.prototype = Object.create(this.Animatable.prototype);
    this.Unit.constructor = this.Animatable;
    
    /**
     * Player This object represents a player real or virtual.  It holds all player's units/cities, and uses events to manage turns
     * @param attributes an object containing any attributes this opject should posses
     * @return void
     */
    this.Player = function(attributes){
        obj.Interactive.call(this,attributes);
        
        //Private Variables
        var o = this;
        var units=[];
        var cities = [];
        var activeIndex,activeUnits;
        
        o.attr('onturn',function(e){
            //Set this player active
            app.activePlayer = o.attr('id');  
            
            //Setup a list of active units for this turn
            activeUnits = units.slice(0);//Important because slice creates a new array instead of just adding a reference to the same one.
            if(activeUnits.length > 0){
                activeIndex = 0;
                for(var i=0; i<activeUnits.length; i++){
                    activeUnits[i].resetMovement();
                }
            }
            
        
            console.log(o.attr('name'));
        });
        
        /**
         * Public Method - Create a new unit for this player
         * @param object attributes
         * @return void
         */
        o.newUnit = function(type,address){
            var unit = new obj.Unit(o.attr('id'),type,address);
            if(unit instanceof obj.Unit){
                units.push(unit);
            }
        };
        
        /**
         * Public Method - This method receives commands from the user
         * @param object cmd - holds a list of commands to run
         * @return void
         */
       o.command = function(cmd){                   
            var unit = activeUnits[activeIndex];
            //Turn is over if no active unit
            if(!activeUnits[activeIndex]){
                o.trigger('turn');
                return;
            }
            
            //Work the movement commands first
            var hasMovesLeft = (cmd.chngUnt === 'unitComplete') ? false : true;
            var address = unit.addr().split(':');
            var a0 = parseInt(address[0]);
            var a1 = parseInt(address[1]);
            if(cmd.dir){
                switch (cmd.dir) {
                    case "left":
                        hasMovesLeft = unit.moveTo(a0-1 , a1);
                        break;
                    case "right":
                        hasMovesLeft = unit.moveTo(a0+1 , a1);
                        break;
                    case "upLeft":
                        hasMovesLeft = (a1%2 === 0) ? unit.moveTo(a0-1, a1-1) : unit.moveTo(a0, a1-1);
                        break;
                    case "upRight":
                        hasMovesLeft = (a1%2 === 0) ? unit.moveTo(a0,a1-1) : unit.moveTo(a0+1,a1-1);
                        break;
                    case "downLeft":
                        hasMovesLeft = (a1%2 === 0) ? unit.moveTo(a0-1 , a1+1) : unit.moveTo(a0,a1+1);
                        break;
                    case "downRight":
                        hasMovesLeft = (a1%2 === 0) ? unit.moveTo(a0 , a1+1) : unit.moveTo(a0+1, a1+1);
                        break;
                }
            }
           
            //Deal with Unit Changes
            if(!hasMovesLeft){console.log('splice');
                activeUnits.splice(activeIndex,1);
            }
            
            //Test if move is over
            if(activeUnits.length === 0) o.trigger('turn');
        };
    };
    this.Player.prototype = Object.create(this.Interactive.prototype);
    this.Player.constructor = this.Interactive;
    
    /**
     * Timer This provides a way to manage all time based tasks such as animation and polling of the server
     * @param attributes an object containing any attributes this object should posses
     * @return void
     */
    this.Timer = function(attributes){
        obj.Interactive.call(this,attributes);
        
        //Private Variables
        var interval = setInterval(function(){                                          
            for(var i=0; i<registry['animation'].length; i++){                          
               var animatable = registry['animation'][i];
               if(animatable.attr('framesRemaining') == 0){
                   registry.animation.splice(i,1);
                   animatable.unbind('tween',animatable.attr('ontween'));
                   animatable.trigger('complete');
                   animatable.unbind('complete',animatable.attr('oncomplete'));
               }
               else{
                   animatable.trigger('tween');
                   animatable.attr('framesRemaining',animatable.attr('framesRemaining')-1);
               }
            }
        },this.cfg.framerate);
        
        var registry = {
            animation:[],
            poll:[]
        };
        
        /**
         * Public Method destroy clear the timer and registry
         * @param void
         * @return void
         */
        this.destroy = function(){
            clearInterval(interval);
        };
        
        /**
         * Public Method register an animation object
         *  @param animation object instance of Animation
         *  @return void
         */
        this.animate = function(animation){
            //Not animatable you're outa here
            if(!animation instanceof obj.Animatable){
                return;
            }
            
            registry.animation.push(animation);
        };
    };
    this.Timer.prototype = Object.create(this.Interactive.prototype);
    this.Timer.constructor = this.Interactive;
    
}).apply(app.objLib);
