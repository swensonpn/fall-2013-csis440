/**
 * FUNCTIONLIBRARY This is the location of all utility functions that I need to
 * write.  They may be used in the game or in the widgets.
 */
(function(){
    /**
     * Public Function createCoordHashTable creates a hash table representantion
     * for x by y dimensions.
     * @param dimX the number of columns
     * @param dimY the number of rows
     */
    this.createCoordHashTable = function(dimX,dimY){
        var ht={},x,y,l=(dimX*dimY);
       
        for(var r=0; r<dimX; r++){
            for(var c=0; c<dimY; c++){
                ht[c + ',' + r] = null;
            }
        }
        return ht;
    };
    
    /** Public function getAddrForHexCoords get the address of an item from its pixel coords
     * @param d diameter of the object
     * @param x the x pixel location of the object
     * @param y the y pixel location of the object
     * @return address string
     */
    this.getAddrForHexCoords = function(d,x,y){//Hex grid only
        
    };
    
    /**
     * Public function getHexCoordsForAddr get the xy pixel location of an object based on it's address
     * @param d diameter of the object
     * @param addr the address of the object
     * @return array where arr[0] is x and arr[1] is y
     */
    this.getHexCoordsForAddr = function(d,addr){//Hex grid only
        var r=d/2,c=addr.split(':');
        if(c[1]%2){
            return [c[0]*d+d,c[1]*(d*3/4)+r];
            //return[c[0]*d,c[1]*(d*3/4)];
        }
        else{
            return [c[0]*d+r,c[1]*(d*3/4)+r];
            //return [c[0]*d+r,c[1]*(d*3/4)];
        }
    };
    
    /**
     * Public function getRandomArrayValue grab a random item from a supplied array.
     * @param arr the source array
     * @return mixed random value from array
     */
    this.getRandomArrayValue = function(arr){
        return arr[Math.floor(Math.random() * arr.length)];  
    };
    
    /**
     * Public function handles keystroke events
     * @param e event object
     * @void 
     */
    this.keyHandler = function(e){
        var data = {};
        switch (e.keyCode) {
            case 97://1=97
                data.dir = 'downLeft';
                break;
            case 98://2=98
                data.chngUnt = 'unitLast';
                break;
            case 99://3=99
                data.dir = 'downRight';
                break;
            case 100://4=100
                data.dir = 'left';
                break;
            case 101://5=101
                data.chngUnt = 'unitComplete'
                break;
            case 102://6=102
                data.dir = 'right';
                break;
            case 103://7=103
                data.dir = 'upLeft';
                break;
            case 104://8=104
                data.chngUnt = 'unitNext';
                break;
            case 105://9=105
                data.dir = 'upRight';
                break;
        }
        console.log('keyup for ' + app.activePlayer + ' ' + app.players[app.activePlayer].attr('name'));
        app.players[app.activePlayer].command(data);
    };
    
}).apply(app.funcLib);