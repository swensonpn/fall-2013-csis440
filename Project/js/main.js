/**
 * @author Paul N Swenson
 * @version 1.0
 * This file contains the project's primary namespace, and other items that
 * don't easily fit in other locations.  In a production environment we would
 * combine all the .js files into one and run the code through a minimizer.  
 * This is usually done by a utility program like closure, which removes 
 * whitespace,comments,and shortens variable names.
 */
 
 /**
  * NAMESPACE This will be the only variable accessible to the global scope.  As
  * much as possible we try to keep this clean, but some variables need to be
  * decared in this scope.  There is also a function initialize that acts as a
  * type of constructor setting up properites as necessary.
  */
var app = {
        //Sub-namespaces
        config:{},
        objLib:{},
        funcLib:{},
        widgets:{},
        
        //Properties of the primary namespace
        map:null,
        timer:null,
        activePlayer:null,
        players:[],
        
        //The constructor for our namespace
        initialize:function(){
           
            //Build Sprite Sheets
            for(var n in app.config.spriteSheets){
                app.config.spriteSheets[n] = new app.objLib.SpriteSheet(app.config.imagePath + app.config.spriteSheets[n].file, app.config.spriteSheets[n].sprites);
            }
            
            //Grab the canvas elements and initialize them
            for(i=0; i<app.config.canvases.length; i++){
                var canvas = document.getElementById(app.config.canvases[i]);
                canvas = canvas.getContext('2d');
                app.config.canvases[i] = canvas;
            }
            
            //Build and populate the hashMap
            var w = app.config.map.width;
            var h = app.config.map.height;
            app.map = app.funcLib.createCoordHashTable(w,h);
            for(var r=0; r<w; r++){
                for(var c=0; c<h; c++){
                    var type = app.funcLib.getRandomArrayValue(app.config.tTypeArr);
                    var address = r + ':' + c;
                    app.map[address] = new app.objLib.Terrain(type,address);
                }
            }
            
            //Create the players
            app.players[0] = new app.objLib.Player({id:0,name:'player 1'});
            app.players[1] = new app.objLib.Player({id:1,name:'player 2'});
            
            //Bind Player Turn Events
            app.players[0].bind('turn',app.players[1].attr('onturn'));
            app.players[1].bind('turn',app.players[0].attr('onturn'));
            
            //Attach the keystroke handler
            window.addEventListener('keyup',app.funcLib.keyHandler,false);
            
            //Start the game timer
            app.timer = new app.objLib.Timer();
        },
        startGame:function(){
            
            //Render the map
            for(var n in app.map){
                if(app.map[n] !== null) app.map[n].render();
            }
            
            //Inject one unit for each player
            app.players[0].newUnit('infantry','0:0');
            app.players[0].newUnit('armor','0:1');
//            app.players[1].newUnit('infantry', (app.config.map.width-1)+':'+(app.config.map.height-1));
            app.players[1].newUnit('infantry','12:12');
            app.players[1].newUnit('armor','12:11');
            
            //Start the Players Turn
            app.activePlayer = 0;
            app.players[1].trigger('turn');
            
            
//            var x = new app.objLib.Unit(1,'infantry','0:0');
           //  x.addr('7:2');
           
//           x.animateTo('0:1');
             
            return 'Started';
        }
    };

/**
 * MAIN APPLICATION CODE This self-executing function will hold all code that cannot
 * be encapsulated into some type of reusable object.  It will also control
 * initial page actions on load.
 */
(function(app){
    window.onload = function(){
        app.initialize();
        
       setTimeout(function() {
           app.startGame();
       }, 1000);
   /*     
        //test case code
        var x = new app.objLib.Terrain('grass');
           if(x instanceof app.objLib.Base){
               console.log(x.attr('move'));
               app.timer.register(function(){console.log(x.attr('move')*10);});
           }
           */
    };
}(app));