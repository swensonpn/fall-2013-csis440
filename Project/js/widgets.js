/**
 * WIDGETS These are special objects that manipulate the html page.  Typically,
 * we want to design things like menus as stand alone code segments allowing
 * for reuse of the code.  In this project the main game application may depend
 * on the presence of one or more widgets.  
 */
 
/**
 * MAIN_MENU This widget does not reside in the app namespace.  It is
 * completely independant and is protected from the global scope by
 * a self-executing anonymous function
 */
(function(linkSelector){
    var toggleLink,menuList;
    var framerate = 20;
    var tweenType = [1,2,3,4,5,6,7,8,9,10,9,8,7,6,5,4,3,2,1];//sum must be 100
    var tweenIndex = 0;
    var tweenDirection = -1;
    var menuHeight = 0;
    var actualMenuHeight = 0;
    
    //This handler function called each time the link is clicked.
    var toggleLinkHandler = function(e){
        tweenIndex = 0;
        tweenDirection = (tweenDirection === 1) ? -1 : 1;
        
        e.preventDefault();
        //start animation
        setTimeout(doTween, framerate);
    };
    
    //This function called every framerate(20) milliseconds until animation complete.
    var doTween = function(){
        //Ensure animation isn't over
       if(tweenIndex < tweenType.length){
           //recalculate height
            menuHeight = (tweenType[tweenIndex] * 0.01 * actualMenuHeight * tweenDirection) + menuHeight;
            menuList.style.height = menuHeight + 'px';
           
            //uncomment to see height change
            //console.log(menuList.style.height);   

            tweenIndex++;
            setTimeout(doTween,framerate);
       }
       else{
            //push to full or no height when done
            menuList.style.height = (tweenDirection === 1)?  actualMenuHeight + 'px' : '0px';
       }
    };
    
    //Widget self-constructs on window load
    window.addEventListener('load',function(){
        //Get out of here if window is too wide
        if(window.innerWidth > 768){
            return;
        }
        
        //get the link and menu
        toggleLink = document.querySelector(linkSelector);  
        menuList = document.querySelector(toggleLink.getAttribute('href'));
        
        //Output error messages if selectors fail to find required html
        if(toggleLink === null){
            console.log("Toggle link not found");
            return;
        }
        else if(menuList === null){
            console.log("Main Menu not found");
            return;
        }
        
        //Make sure the main menu is not hidden, but has zero as a height
        menuList.style.height = '0px';
        menuList.style.display = 'block';
        menuList.style.overflow = 'hidden';
        
        //Get the actual height of the menu
        actualMenuHeight = menuList.scrollHeight;
        
        //add event listener for link
        toggleLink.addEventListener('click',toggleLinkHandler, false);
    },false);
}('.menu-toggle'));  
    

/**
 * GAME_PANEL This widget does not reside in the app namespace.  The
 * game relies on html in these panels, but the html is static.
 */
 (function(panelId){
    var tabs,panels;
    var framerate = 20;
    var tweenType = [1,2,3,4,5,6,7,8,9,10,9,8,7,6,5,4,3,2,1];//sum must be 100
    var tweenIndex = 0;
    var currentOpacity = 1;
    var activeTabIndex = 0;
    var activePanel = undefined;
    var fadeDirection = -1;
     
    //Executed when a tab gets clicked
    var tabLinkHandler = function(e){
        e.preventDefault();
        fadeDirection = -1;
        currentOpacity = 1;
        tweenIndex = 0;
        
        //update tab styles
        for(var i=0; i<tabs.length; i++){
            if(tabs[i] === this){
                tabs[i].parentNode.className = 'active';
                activeTabIndex = i;
            }
            else{
                tabs[i].parentNode.className = '';
            }
        }
        setTimeout(doTween, framerate);
    };
    
    //This executes each frame of the animation
    var doTween = function(){
        
        //Ensure animation not over
        if(tweenIndex < tweenType.length){
            currentOpacity = (tweenType[tweenIndex]  * 0.01 * fadeDirection) + currentOpacity;
            activePanel.style.opacity = currentOpacity;
            
            //uncomment to unbug
            //console.log((currentOpacity*100) + '%');
            
            tweenIndex++;
            setTimeout(doTween,framerate);
        }
        else if(fadeDirection === -1){
            fadeDirection = 1;
            tweenIndex = 0;
            currentOpacity = 0;
            
            //remove display of old tab
            activePanel.style.display = 'none';
            
            //switch from old tab to new tab
            activePanel = panels[activeTabIndex];
            
            //Set starting styles
            activePanel.style.opacity = 0;
            activePanel.style.display = 'block';
            
            setTimeout(doTween,framerate);
        }
    };
     
    //Widget self-constructs on window load
    window.addEventListener('load',function(){
        //Get the tabs
        tabs = document.querySelectorAll(panelId + ' a');
        panels = [];
        for(var i=0; i<tabs.length; i++){
            //get the panel that matches this link
            var panel = document.querySelector(tabs[i].getAttribute('href'));
            if(panel === null){
                console.log(tabs[i].getAttribute('href') + ' panel not found');
                return;
            }
            
            //Attach an event listener for the link getting clicked
            tabs[i].addEventListener('click',tabLinkHandler,false);
            panels.push(panel);
            
            
            //Set up initial active tab
            if(i === 0){
                tabs[i].parentNode.className = 'active';
                panels[i].style.display = 'block';
                activePanel = panels[i];
            }
            else{
                panels[i].style.display = 'none';
            }
        }
         
    },false);
 }('#game-panel>section'));
 
 
 
 
  