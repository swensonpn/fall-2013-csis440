/*
 * Variable Naming Convensions
 * - Must begin with a letter, dollar sign, or underscore.
 * - They are case sensitive
 * - You may not use JavaScript reserver words (using 'if' would be an error)
 * - Use Camel Case style
 *      myVariable //for variables
 *      MyClass //for object definitions/classes
 *      CONSTANT //for Constants
 */
 
/*
 * Variable Scope
 */
var myVariable = 1; //using the keyword var indicates a local variable (NOTE:All variable declarations should be done with var.)
myNextVariable = 2; //Omitting var indicates the variable is global
window.myNextNextVariable = 3; //All global variables are members of the window object

//Constants
const MYCONSTANT = 5; //The value of a constant must be set at the same time it is declared.
                      //Scope is determined by where the constant is declared (in a function is local)

//Example of Variable Scope
var var1 = 0,
    var2 = 0;//Both var1 and var2 are declared in the global scope
(function example(){//Look at curly braces to determine scope
    var var1 = 5;//This only exists inside the function 'example'
    var2 =6;//JavaScript looks inside the function first then outside the function until it finds var2
    
    console.log(window.var1);
    console.log(window.var2);
}());

/*
 * Data Types
 */
 
//String
var str1 = "any characters enclosed in single or double quotes";
var str2 = 'Paul\'s string'; //If you need to use a quote of the same type in your string escape it with a backslash

//Boolean
var bool1 = true;//Can be true or false

//Number
var num1 = 5.342; //Can be used to represent integers and floats, in many different formats.  Has special values for infinity and not a number

//Null Undefined Empty
var undefinedVar;//This variable has been declared but not given a value so it is undefined
var nullVar = null;//This variable has been set to 'no value'
var emptyVar = "";//This is a string with no characters.  Not the same as null or undefined



