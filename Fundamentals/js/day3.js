/**
 * AGENDA
 * 1. Activity 3
 * 2. Example Proposal
 * 3. Lecture
 * 4. Homework
 */
 
 
/**
 * Declarative Functions
 * 
    function is a keyword that indicates this is a function (required).
    the return statement is optional.
    there can be 0 or more parameters.
    the live scope of the parameter is limited to the function itself.
    params of a primitive data type are passed by data.
    params of an object data type are passed by reference
    declarative functions are parsed once when it is used for the first time
*/
function rollDice(n){
    var totalRoll = 0;
    for(var i=0; i<n; i++){
        totalRoll += Math.round(Math.random() * 6);
    }
    return totalRoll;
}
console.log(rollDice(2));

function attack(attackePowerr,defenderPower){
    if((attackePowerr*Math.random()) > (defenderPower * Math.random())){
        return true;
    }
    else{
        return false;
    }
}
console.log('attacker wins ' + attack(5,4));

/**
 * Literal Functions
 * 
    Have characteristics of both declarative and anonymous functions.
        Like anonymous functions this type of function has no declared name.
        Like declarative functions they are only parsed once.
    Can be assigned to a variable, but do not have to be.
    Can be passed like any other variable
    Call the function with it's variable name (myFunc(arg1,arg2,...);)
 */
 var turn = function(moveType){
    switch (moveType) {
        case "roll":
            return 'You rolled ' + rollDice(1);
        case "attack":
            return 'Your attack ' + attack(1,2);
        default:
            return "That is not a valid choice."
    }
 };
//var moveType = prompt("What do you want to do?");
// console.log(turn(moveType));
 
//write function turn use prompt/switch to pass a func


/**
 * Anonymous Functions
 * 
    Functions are object in JavaScript so you can create them with its constructor.
    Like with declarative function you may have 0 or more params, but in the constructor the last parameter will be the body of the function you are creating.
    An anonymous function is parsed each time it is invoked.
    Used in cases where the function definition is not known beforehand.
 */
 //function fn1(msg){alert(msg);}
 var fn1 = new Function('msg','alert(msg);');
 //fn1("Hello World");
 
 
/** 
 * Self-Calling Functions
 *  (function(param1){code to execute}(arg1))
 */
 
(function(){
    console.log("Hi I can call myselsf");
}());


/**
 * Function Properties
 *  - length
 *  - arguments (JavaScript does not support overloading. arguments can be used to simulate)
 */
 var fn2 = function(){
     for(var i = 0; i<arguments.length; i++){
         if(i == 0 && typeof arguments[i] == 'string'){
             console.log('arg 1 is a string');
         }
         console.log(arguments[i]);
     }
 };
 fn2('param1','param2','param3');
 
 
/**
 * Nested Functions
 *  
    The inner function operates within the scope of the outer function
        This means the inner function has access to the outer functions local variables
        Global variables and functions are available to both the inner and outer functions
        The outer function does not have access to local variables inside the inner function.
    The inner function can be called anywhere inside the outer function
    The inner function cannot be called from outside the outer function.
    The outer function can expose the inner function to the global scope by returning the inner function. 
        Remember that functions are first members in JavaScript, and can be assigned to/passed as variables
        Syntax:  function outer(){var x = 1; function inner(){code}; return inner;}
        Normally, all arguments/local variables of both the inner and outer functions die on completion of the function call.  However, when the inner function is returned all arguments/local variables of both the inner and outer function still exist.  This is called closure.
 */
var globalVar = 'global';
function outer(){
    var outerVar = 'outer';
    function inner(){
        var innerVar = 'inner';
        return globalVar + outerVar + innerVar;
    }
    return inner();
}
console.log(outer());

function outer1(){
    return function(){
        return 'works';
    }
}
var returnedFunc = outer1();
console.log(returnedFunc());


 /** 
  * Example: Consider Game Boards
  */