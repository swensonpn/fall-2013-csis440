/**
 * TODO talk about activity 3
 */

//CONDITIONAL STATEMENTS
var arr1 = new Array("red","yellow","blue","green","purple","orange");

//Or in variable assignment
var option1 = arr1[10]  || 'black';
console.log('or in assignment=' + option1);

//Ternary (only one statement allowed)
var option2 = (arr1[0].length == 3) ? 'red' : 'not red';
console.log(option2);

//If statement (curly braces are optional, but only one statement will run without them)
if(arr1.length == 5){
    console.log('first statement');
    console.log('second statement');
}

//Else If (you can have as many 'else if' expressions as you need)
if(arr1[0] == 'blue'){
    console.log('blue true');
}
else if(arr1[1] == 'black'){
    console.log('black true');
}
else if(arr1[0] == 'red'){
    console.log('red true');
}
else{
    console.log('else');
}

//Switch (can evaluate strings or numbers, 'default' is optional, must use 'break' to prevent default code from executing)
var switchOut;
switch(arr1[0]){
    case "black":
        switchOut = 'black';
        break;
    case "red":
        switchOut = 'red';
        break;
    default:
        switchOut = undefined;
}
//console.log('switch out = ' + switchOut);

//LOOPS
//While
var counter = 0;
while(counter < 4){
    counter++;
}
console.log(counter);

//Do While
do{
    counter++;
}while(counter < 6);
console.log(counter);

//For
for(var i=0; i<arr1.length; i++){
   // console.log(arr1[i]);
}

//For In (objects)
for(var n in window){
    //console.log(n + ':' + window[n]);
}

//ARRAYS (in JavaScript indexed by integer)
//Constructing (Not fixed length)
var arr2 = new Array();
var arr3 = [];

//Setting and Getting values (elements that are out of bounds return undefined)
arr2[0] = 'tiger';
arr2[3] = 'lion';

var value1 = 'The ' + arr2[0] + ' is hungry';
console.log(value1); 

//Properties and methods (http://www.w3schools.com/jsref/jsref_obj_array.asp)


//BUILT-IN Objects
//Boolean (http://www.w3schools.com/jsref/jsref_obj_boolean.asp)

//Number (http://www.w3schools.com/jsref/jsref_obj_string.asp)
//talk about parsing and casting

//String (http://www.w3schools.com/jsref/jsref_obj_string.asp)
var str1 = "This is a test";
var arr4 = str1.split(' ');
console.log(arr4);

//Date (http://www.w3schools.com/jsref/jsref_obj_date.asp)

//Math(http://www.w3schools.com/jsref/jsref_obj_math.asp)


//USING MATH TO CREATE GAME CONSTRUCTS

//Dice
console.log(Math.round(Math.random() * 6));

//Cards
var cards = ["Jack","Queen","King","Ace"];
while(cards.length > 0){
    var num = parseInt(Math.random() * cards.length);
    var card = cards.splice(num,1);
    console.log(card.toString());
}


//Attack Defend
var attacker = 1;
var defender = 2;