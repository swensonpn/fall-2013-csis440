/**
 * AGENDA
 *  1. Homework Questions?
 *  2. Add fixed length animation to our widget
 *  3. Error Handling
 *  
 */
//By Default JavaScript has this setting
 window.onerror = null;
 
 
 //Capturing the Error Event
window.addEventListener('error',function(){
    console.log('Oops. We have an error.')    
},false);
 
// Preventing the default 
window.onerror = function(msg,url,line){
    console.log('Error Line: ' + line);
    console.log('Error File: ' + url);
    console.log('Error Message: ' + msg);
    return true;  
};

 
 /* THE DEFAULT ERROR COMMENT OUT TO SEE TRY CATCH */
 //var variable = thisVariableDoesNotExist;
 
 /* VERIFY CODE STOPS AT THE ERROR */
 console.log('Next statement after the error demonstrates that the error has been handled.');
 
 
 // Using a try catch
 try{
     //Section of code we want to run with control over how errors affect flow
     console.log('First statement in try');
     var variable = undeclaredValue;//This is the error
     console.log('last statement in try');
 }
 catch(err){
     console.log("Oops.  Error in try");
     for(var n in err){
         console.log(n + ': ' + err[n]);
     }
 }
 
 /* VERIFY CODE CONTINUES AFTER TRY CATCH */
 console.log('Next statement after the error demonstrates that the error has been handled.');
 
 //Using throw with try catch
 var test;
 try{
     //Using throw with try catch can be one way to capture edge cases and fix them.
     if(!test){
         throw {error:'Undefined Variable',solution:"Hello this message is more appropriate than undefined"};
     }
 }
 catch(err){
     console.log('Oops. ' + err.error + ' correcting now.');
     test = err.solution;
 }
 
 /* VERIFY ERROR IS FIXED */
 console.log(test);
 
