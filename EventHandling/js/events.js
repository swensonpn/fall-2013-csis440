/**
 * AGENDA
 *  1. finish DOM
 *  2. Event Hangling
 */
 
/* Build a mouseover/mouseout event */
function myMouseOver(el){
    el.style.color = "green";
}
function myMouseOut(el){
    el.style.color = "";
}

/* Build it a better way. Backward Compatible */
var handler = function(){
    alert('Stop Execution. Look at the list item');
    var elem = document.getElementById('backaward');
    elem.style.color = "blue";
    elem.style.fontWeight = "bold";
};

if(window.addEventListener){//All except IE8 and below
    window.addEventListener('load',handler,false);
}
else if(window.attachEvent){//Ie 6-8
    window.attachEvent('onload',handler);
}
else{
    window.onload = handler;
}

/* Many developers are removing support for older IE so we can just do this*/
window.addEventListener('load',function(){
    document.getElementById('today').addEventListener('click',function(){
        //document.getElementById('today').style.color = "brown";
        this.style.color = "orange";
    },false);
},false);

/* Events travel up and down the DOM node Tree */  
function demoBubble(){
    window.addEventListener('click',function(){console.log("I am the window and I heard click.")},false);
    document.querySelector('.parent').addEventListener('click',function(){console.log("I am the parent and I heard click.")},true);
    document.querySelector('.child').addEventListener('click',function(){console.log("I am the child and I heard click.")},false);
}
/* What is an Event Handler 
 *  - Any function we pass into the event listener.  It receives one parameter object event
 *
 * Inspect the Event Object 
 */
window.addEventListener('x',function(e){
    for(var n in e){
        console.log("The propert " + n + " = " + e[n]);
    }
},false);

/* Key up event */
window.addEventListener('keyup',function(e){
    switch(e.keyCode){
        case 32:
            console.log('space');
    }
},false);

/* Drag and Drop */
function demoDragDrop(){
    var tgt1 = document.getElementById('dropzone1');
    var tgt2 = document.getElementById('dropzone2');
    var source = document.getElementById('drag');
    
    var dropHandler = function(e){
        e.preventDefault();
        var data=e.dataTransfer.getData("sourceText");
        e.target.appendChild(document.getElementById(data));
    };
    
    var dragOverHandler = function(e){
       e.preventDefault();
    };
    
    var dragStart = function(e){
        e.dataTransfer.setData("sourceText",e.target.id);
    };
    
    tgt1.addEventListener('drop',dropHandler,false);
    tgt2.addEventListener('drop',dropHandler,false);
    tgt1.addEventListener('dragover',dragOverHandler,false);
    tgt2.addEventListener('dragover',dragOverHandler,false);
    source.addEventListener('dragstart',dragStart,false);
}