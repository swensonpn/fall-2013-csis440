/**
 * AGENDA
 *  1. Homework 4
 *  2. Finish Events
 *  3. Use DOM and Events to build a widget
 *      a. HTML and CSS (Do as much as possible here)
 *      b. Bulid Self-Executing/self-constructiong container
 *      c. Get references to any DOM elements used more than once.
 *      d. Setup Events without animation
 *      e. Add animation
 */
 
//Self-Executing Function
(function(id){
    var menuToggle,menuList;
    var frameRate = 20;
    var actualMenuHeight = 0;
    var animationDirection = 1;
    
    var animationScheme = [1,2,3,4,5,6,7,8,9,10,9,8,7,6,5,4,3,2,1];
    var currentHeight = 0;
    var currentIndex = 0;
    
    var toggleHandler = function(e){
        e.preventDefault();
        doTween();

    };
    
    var doTween = function(){
        if(currentIndex < animationScheme.length){
            //console.log(animationScheme[currentIndex]);
            currentHeight += animationDirection *  actualMenuHeight * animationScheme[currentIndex] * 0.01;
            //console.log(animationScheme[currentIndex] + ' : ' + currentHeight);
            menuList.style.height = currentHeight + 'px';
            
            currentIndex++;
            setTimeout(doTween, frameRate);
        }
        else{
            currentIndex = 0;
            if(animationDirection > 0){
                animationDirection = -1;
                currentHeight = actualMenuHeight;
            }
            else{
                animationDirection = 1;
                currentHeight = 0;
            }
        }
    };
    
    window.addEventListener('load',function(e){
        menuToggle = document.getElementById(id);
        
        if(menuToggle === null){
            console.log("There is no element with an id equal to " + id);
            return;
        }
      
        menuList = document.querySelector(menuToggle.getAttribute('href'));
        
        if(menuList === null){
            console.log("There is no menu with an id equal to  " + menuToggle.getAttribute('href'));
            return;
        }
        actualMenuHeight = menuList.scrollHeight;
        menuList.style.height = '0px';
        menuToggle.addEventListener('click',toggleHandler,false);
    },false);
}('main-menu-toggle'));
    //Initialize on page load